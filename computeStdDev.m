function [average,stdDev] = computeStdDev(inputMatrix)

    % Compute mean
    average = computeAverage(inputMatrix);

    % Compute numerator
    [m,n] = size(inputMatrix);

    sum = 0;
    for i = 1:m
        for j = 1:n
           sum = sum + (inputMatrix(i,j) - average)^2; 
        end
    end
    % compute standard deviation
    stdDev = sqrt(sum/(m*n-1));



end

