function averageVal = computeAverage(inputMatrix)
    % Returns the average value of a matrix input

    % Find size of matrix
    [m,n] = size(inputMatrix);
    % Sum all elements
    sum = 0;
    for i = 1:m
        for j = 1:n
           sum = sum + inputMatrix(i,j); 
        end
    end
    % compute average vale
    averageVal = sum/(i*j);
end