clear
clc

% Generate a matrix of ones
A = ones(20,20);
% Compute average value
average = computeAverage(A)

% Test a random matrix
B = rand(4,4)
% Compute average value
average2 = computeAverage(B)
