clear
clc

% Generate a matrix of ones
A = ones(3,4);
% Compute average value
[average,stdDev] = computeStdDev(A)